
export const state = () => ({
  projects: [
    {
      id: 1,
      title: 'Dikadoin Dong',
      link: 'https://qitadev.github.io/dikadoin-dong/',
      image: 'Dikadoin.png',
      category: 'Front End Development',
      build: ['React', 'TailwindCSS', 'Gatsby']
    },
    {
      id: 2,
      title: 'RA Yasdjanur',
      link: 'https://rayasdjanur.sch.id/',
      image: 'yasdjanur.png',
      category: 'Front End Development',
      build: ['Vue', 'TailwindCSS', 'Nuxt']
    },
    {
      id: 3,
      title: 'JALA Web Apps',
      link: 'https://app.jala.tech/',
      image: 'jala.png',
      category: 'Front End Development',
      build: ['JS', 'Vue', 'Sass', 'Laravel']
    },
    {
      id: 4,
      title: 'PokePedia',
      link: 'https://firdausreza-pokepedia.vercel.app/',
      image: 'PokePedia.png',
      category: 'Front End Development',
      build: ['JS', 'Vue', 'TailwindCSS', 'GraphQL', 'Apollo', 'PokeAPI']
    },
    {
      id: 5,
      title: 'Gramedia',
      link: 'https://gramedia.com/',
      image: 'Gramedia.png',
      category: 'Front End Development',
      build: ['Angular', 'TypeScript', 'Less']
    },
  ]
})

export const getters = {
  get (state) {
    return state.projects
  }
}
