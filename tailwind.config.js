module.exports = {
  mode: 'jit',
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'primary-bg': '#000235',
        'ellipse-1': '#8D8DDA',
        'ellipse-2': '#AAD9D9',
        'dblue': '#1F2235',
        'dlblue': '#24263B',
        'lred': '#FF4A57',
        'c-purple': '#5E5ED4',
        'custom-brown': '#663300',
        'custom-orange': '#CC6600',
        'yellow-orange': '#333300',
        'yellow-green': '#CC9900'
      },
      width: {
        200: '200px',
        300: '300px',
        400: '400px',
        500: '500px'
      },
      height: {
        200: '200px',
        300: '300px',
        400: '400px',
        500: '500px'
      },
      screens: {
        vsm: { min: '425px' },
        'six-inch': { min: '510px' },
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
